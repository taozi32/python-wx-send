import os
import sys
import string
import mongo
from utils.Message import Message

class MyTask:
	def __init__(self):
		self.start = 1
		self.stop = 0

	def task(self):
		return

	def run(self, stri):
		self.colorText("task has running: " + stri, 'success')
	
	def colorText(self, text, ctype):
		if ctype == 'success':
			print('\033[1;32;40m')
		else :
			print('\033[1;31;40m')

		print text
		print('\033[0m')

if __name__ == '__main__':
	mytask = MyTask()
	message = Message()
	try:
		task_name = sys.argv[1]
	except Exception, e:
		mytask.colorText("Task name required!\r\n", 'error')
		sys.exit(0)
	mytask.run(task_name)
	mytask.colorText(message.sayHello(), 'error')
